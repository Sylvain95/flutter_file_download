import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:dio/dio.dart';
import 'package:external_path/external_path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:open_file/open_file.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Files download',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController linkUrl =   TextEditingController();
  var dio  = Dio();
  bool _loading = false;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    requestPermission();
    var androidSettings = const AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOSSettings = const IOSInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );

    var initSettings = InitializationSettings(android: androidSettings,iOS: iOSSettings);
    flutterLocalNotificationsPlugin.initialize(initSettings,onSelectNotification: onClickNotification);
  }

  Future onClickNotification(String? json) async{
    final objJson = jsonDecode(json!);

    if(objJson['isSuccess']){
      OpenFile.open(objJson['filePath']);
    }else{
      showDialog(context: context,
          builder: (_)=> const AlertDialog(
            title: Text("Error"),
            content: Text("Download failled"),
          )
      );

    }
  }

  showNotifcation(Map<String,dynamic> domnloadStatus) async{
    final json = jsonEncode(domnloadStatus);
    var androidDetails = const AndroidNotificationDetails("channelId",
        "channelName",
        importance: Importance.max
    );
    var iOSDetails = const IOSNotificationDetails();
    var platformDetails = NotificationDetails(android: androidDetails,iOS: iOSDetails);
    await flutterLocalNotificationsPlugin.show(0, "title", "body", platformDetails,payload: json,
    );
  }
  void requestPermission() {
    flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation
    <IOSFlutterLocalNotificationsPlugin>()?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  Future domnloadFunction(Dio dio, String url, String savePath, int idNotification) async{
    Map<String,dynamic> result = {
      'isSuccess':false,
      'filePath':null,
      'error':null,
      'id':null
    };

    result['id'] = idNotification;
    setState(() {
      _loading = true;
    });
    try{
      Response response = await dio.get(
          url,
          onReceiveProgress: showDownloadProgress,
          options: Options(
              responseType: ResponseType.bytes,
              followRedirects: false,
              validateStatus: (status){
                return status! < 500;
              }
          )
      );
      if(response.statusCode == 200){
        setState(() {
          _loading = false;
        });
        result['isSuccess'] = response.statusCode == 200;
        result['filePath'] = savePath;
      }else{
        setState(() {
          _loading = false;
        });
      }

      File file = File(savePath);
      var raf = file.openSync(mode: FileMode.write);
      raf.writeFromSync(response.data);

      await raf.close();
    }catch(e){
      result['error'] = e.toString();
    }finally{
      setState(() {
        _loading = false;
        linkUrl.clear();
        percentDownload = "";
      });

      showNotifcation(result);
    }
  }
  String percentDownload = "";
  void showDownloadProgress(receiver, total){
    if(total != -1){
      print((receiver / total * 100).toStringAsFixed(2)+ "%");
      setState(() {
        percentDownload = (receiver / total * 100).toStringAsFixed(2)+ "%";
      });
    }
  }

  void getPermission() async{
    Map<Permission, PermissionStatus> permissions = await [
      Permission.storage,
    ].request();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _loading? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(percentDownload, style: const TextStyle(color: Colors.black)),
                const SizedBox(height: 10),
                const CircularProgressIndicator(color: Colors.pink),
              ],
            )
        )
            :
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(5),
              child: Container(
                margin: const EdgeInsets.all(15),
                child: TextFormField(
                  maxLines: 3,
                  controller: linkUrl,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(10.0)
                      ),
                      fillColor: Colors.grey,
                      hintText: "Url",
                      focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.pink, width: 2),
                          borderRadius: BorderRadius.circular(10.0)
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.pink[400]!, width: 2),
                          borderRadius: BorderRadius.circular(10.0)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(10.0)
                      )
                  ),
                ),
              ),
            ),
            Padding(padding: const EdgeInsets.all(15),
              child: GestureDetector(
                child: Container(
                  width: 350,
                  color: Colors.pink,
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const [
                      Icon(Icons.file_download,color: Colors.white,size: 15,),
                      SizedBox(width: 10,),
                      Text("Download File",style: TextStyle(color: Colors.white),)
                    ],
                  ),
                ),

                onTap: () async{
                  if(linkUrl.text.isNotEmpty){
                    print("lll");
                    final dir = await ExternalPath.getExternalStoragePublicDirectory(
                        ExternalPath.DIRECTORY_DOWNLOADS
                    );

                    bool isPermission = await Permission.storage.isGranted;

                    setState(() {
                      if(isPermission){
                        final fullPath = path.join(dir.toString(),
                            linkUrl.text.substring(linkUrl.text.lastIndexOf("/") +1));
                        domnloadFunction(dio, linkUrl.text, fullPath, 1);
                      }else{
                        getPermission();
                        final fullPath = path.join(dir.toString(),
                            linkUrl.text.substring(linkUrl.text.lastIndexOf("/") +1));
                        domnloadFunction(dio, linkUrl.text, fullPath, 1);
                      }
                    });
                  }
                },
              ),
            )
          ],
        )
    );
  }

}
